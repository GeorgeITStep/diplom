﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Domain.Core.Base
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
