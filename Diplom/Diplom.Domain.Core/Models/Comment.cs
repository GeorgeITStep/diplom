﻿using Diplom.Domain.Core.Base;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Domain.Core.Models
{
    public class Comment : BaseEntity
    {
        public string Text { get; set; }
        public Book Book { get; set; }
        public IdentityUser User { get; set; }
        public void Copy(Comment item)
        {
            if(item.Text != null)
            {
                this.Text = item.Text;
            }
        }
    }
}
