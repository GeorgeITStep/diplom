﻿using Diplom.Domain.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Domain.Core.Models
{
    public class Biography : BaseEntity
    {
        public DateTime DateOfBirth { get; set; }
        public DateTime DateOfDeath { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Aliases { get; set; }
        public string Language { get; set; }
        public string Occupation { get; set; }
        public string Text { get; set; }
        public Author Author { get; set; }
        public Biography()
        {
        }
        public void Copy(Biography item)
        {
            if(item.DateOfBirth != null)
            {
                this.DateOfBirth = item.DateOfBirth;
            }
            if(item.DateOfDeath != null)
            {
                this.DateOfDeath = item.DateOfDeath;
            }
            if(item.PlaceOfBirth != null)
            {
                this.PlaceOfBirth = item.PlaceOfBirth;
            }
            if(item.Aliases != null)
            {
                this.Aliases = item.Aliases;
            }
            if(item.Language != null)
            {
                this.Language = item.Language;
            }
            if(item.Occupation != null)
            {
                this.Occupation = item.Occupation;
            }
            if(item.Text != null)
            {
                this.Text = item.Text;
            }

        }
    }
}
