﻿using Diplom.Domain.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Domain.Core.Models
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }
        public Category()
        {
        }
        public void Copy(Category item)
        {
            if(item.Name != null)
            {
                this.Name = item.Name;
            }
        }
    }
}
