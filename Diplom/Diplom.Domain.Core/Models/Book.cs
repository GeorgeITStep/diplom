﻿using Diplom.Domain.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Domain.Core.Models
{
    public class Book : BaseEntity
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string PubblishingHouse { get; set; }
        public int Year { get; set; }
        public string Language { get; set; }
        public int Pages { get; set; }
        public Author Author { get; set; }
        public Category Category { get; set; }
        public Book()
        {
        }
        public void Copy(Book item)
        {
            if(item.Name != null)
            {
                this.Name = item.Name;
            }
            if(item.Price != 0)
            {
                this.Price = item.Price;
            }
            if(item.Description != null)
            {
                this.Description = item.Description;
            }
            if(item.Image != null)
            {
                this.Image = item.Image;
            }
            if(item.PubblishingHouse != null)
            {
                this.PubblishingHouse = item.PubblishingHouse;
            }
            if(item.Year != 0)
            {
                this.Year = item.Year;
            }
            if(item.Language != null)
            {
                this.Language = item.Language;
            }
            if(item.Pages != 0)
            {
                this.Pages = item.Pages;
            }
        }
    }
}
