﻿using Diplom.Domain.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Domain.Core.Models
{
    public class Author : BaseEntity
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Img { get; set; }
        public Author()
        {

        }
        public void Copy(Author item)
        {
            if(item.Name != null)
            {
                this.Name = item.Name;
            }
            if(item.LastName != null)
            {
                this.LastName = item.LastName;
            }
            if(item.Img != null)
            {
                this.Img = item.Img;
            }
        }
    }
}
