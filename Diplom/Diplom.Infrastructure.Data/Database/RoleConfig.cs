﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models
{
    public class RoleConfig
    {
        public const string ADMIN = "admin";
        public const string USER = "user";
        public const string MODERATOR = "moderator";
    }
}