﻿using Diplom.Domain.Core.Models;
using Diplom.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
namespace Diplom.Infrastructure.Data.Database
{
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }
    }
    public class DatabaseContext : IdentityDbContext<ApplicationUser>
    {
        
        public DbSet<Biography> Biographies { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DatabaseContext() : base(@"Data Source=(LocalDb)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\aspnet-Diplom-20200528062110.mdf;Initial Catalog=aspnet-Diplom-20200528062110;Integrated Security=True")
        {

        }
        public static DatabaseContext Create()
        {
            return new DatabaseContext();
        }
    }
    public class DatabaseInitializer : CreateDatabaseIfNotExists<DatabaseContext>
    {
        protected override void Seed(DatabaseContext ctx)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(ctx));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(ctx));

            var adminRole = new IdentityRole(RoleConfig.ADMIN);
            var userRole = new IdentityRole(RoleConfig.USER);
            var moderatorRole = new IdentityRole(RoleConfig.MODERATOR);

            roleManager.Create(adminRole);
            roleManager.Create(userRole);
            roleManager.Create(moderatorRole);

            var admin = new ApplicationUser { UserName = "george.step.it@gmail.com", Email = "george.step.it@gmail.com" };
            var moderator = new ApplicationUser { UserName = "Mod", Email = "mod_2019@gmail.com" };

            userManager.Create(admin, "admin123");
            userManager.Create(moderator, "ABc8@r8*");

            userManager.AddToRole(admin.Id, RoleConfig.ADMIN);
            userManager.AddToRole(admin.Id, RoleConfig.MODERATOR);
            userManager.AddToRole(admin.Id, RoleConfig.USER);

            userManager.AddToRole(moderator.Id, RoleConfig.MODERATOR);
            userManager.AddToRole(moderator.Id, RoleConfig.USER);
            ctx.SaveChanges();
        }
    }
}
