﻿using Diplom.Domain.Core.Models;
using Diplom.Domain.Interfaces.Repository;
using Diplom.Infrastructure.Data.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Infrastructure.Data.Repository
{
    class CommentRepository : BaseRepository<Comment>
    {
        private DatabaseContext context;
        public CommentRepository(DbContext context) : base(context)
        {
            this.context = context as DatabaseContext;
        }

        public override void Append(Comment item)
        {
            throw new NotImplementedException();
        }

        public override Comment Get(int id)
        {
            return context.Comments.FirstOrDefault(p => p.Id == id);
        }

        public override IEnumerable<Comment> GetAll(int cId = 0, int aId = 0)
        {
            return context.Comments.ToList();
        }

        public override void Update(Comment item)
        {
            var comment = Get(item.Id);
            comment.Copy(item);
            context.SaveChanges();
        }
    }
}
