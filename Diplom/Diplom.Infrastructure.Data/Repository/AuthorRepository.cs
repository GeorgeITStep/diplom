﻿using Diplom.Domain.Core.Models;
using Diplom.Domain.Interfaces.Repository;
using Diplom.Infrastructure.Data.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Infrastructure.Data.Repository
{
    class AuthorRepository : BaseRepository<Author>
    {
        private DatabaseContext context;
        public AuthorRepository(DbContext context) : base(context)
        {
            this.context = context as DatabaseContext;
        }
        public override Author Get(int id)
        {
            return context.Authors.FirstOrDefault(p => p.Id == id);
        }
        public override void Append(Author author)
        {
            context.Authors.Append(author);
        }
        public override void Update(Author item)
        {
            var author = Get(item.Id);
            author.Copy(item);
            context.SaveChanges();
        }

        public override IEnumerable<Author> GetAll(int cId = 0, int aId = 0)
        {
            return context.Authors.ToList();
        }
    }
}
