﻿using Diplom.Domain.Core.Models;
using Diplom.Domain.Interfaces.Repository;
using Diplom.Infrastructure.Data.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Infrastructure.Data.Repository
{
    class CategoryRepository : BaseRepository<Category>
    {
        private DatabaseContext context;
        public CategoryRepository(DbContext context) : base(context)
        {
            this.context = context as DatabaseContext;
        }
        public override Category Get(int id)
        {
            return context.Categories.FirstOrDefault(p => p.Id == id);
        }

        public override void Update(Category item)
        {
            var category = Get(item.Id);
            category.Copy(item);
            context.SaveChanges();
        }

        public override void Append(Category item)
        {
            context.Categories.Append(item);
        }

        public override IEnumerable<Category> GetAll(int cId = 0, int aId = 0)
        {
            return context.Categories.ToList();
        }
    }
}
