﻿using Diplom.Domain.Core.Models;
using Diplom.Domain.Interfaces.Repository;
using Diplom.Infrastructure.Data.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Infrastructure.Data.Repository
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private DatabaseContext context;
        private BiographyRepository biographyRepos;
        private AuthorRepository authorRepos;
        private BookRepository bookRepos;
        private CategoryRepository categoryRepos;
        private CommentRepository commentRepos;
        private static IUnitOfWork uow;

        public static IUnitOfWork Instance { get => uow ?? (uow = new EFUnitOfWork()); }
        public BaseRepository<Biography> BiographyRepository
            => biographyRepos ?? (biographyRepos = new BiographyRepository(context));
        public BaseRepository<Author> AuthorRepository
            => authorRepos ?? (authorRepos = new AuthorRepository(context));
        public BaseRepository<Book> BookRepository
            => bookRepos ?? (bookRepos = new BookRepository(context));
        public BaseRepository<Category> CategoryRepository
            => categoryRepos ?? (categoryRepos = new CategoryRepository(context));
        public BaseRepository<Comment> CommentRepository
            => commentRepos ?? (commentRepos = new CommentRepository(context));

        private EFUnitOfWork()
        {
            context = new DatabaseContext();
        }
    }
}
