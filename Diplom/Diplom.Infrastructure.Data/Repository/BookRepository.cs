﻿using Diplom.Domain.Core.Models;
using Diplom.Domain.Interfaces.Repository;
using Diplom.Infrastructure.Data.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Infrastructure.Data.Repository
{
    class BookRepository : BaseRepository<Book>
    {
        private DatabaseContext context;
        public BookRepository(DbContext context) : base(context)
        {
            this.context = context as DatabaseContext;
        }

        public override void Append(Book item)
        {
            throw new NotImplementedException();
        }

        public override Book Get(int id)
        {
            return context.Books.Include("Author").FirstOrDefault(p => p.Id == id);
        }

        public override IEnumerable<Book> GetAll(int cId = 0, int aId = 0)
        {
            if (cId != 0)
            {
                return context.Books.Where(b => b.Category.Id == cId).ToList();
            }
            if (aId != 0)
            {
                return context.Books.Where(b => b.Author.Id == aId).ToList();
            }
            return context.Books.ToList();
        }

        public override void Update(Book item)
        {
            var book = Get(item.Id);
            book.Copy(item);
            context.SaveChanges();
        }
    }
}
