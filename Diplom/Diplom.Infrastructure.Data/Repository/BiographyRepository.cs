﻿using Diplom.Domain.Core.Models;
using Diplom.Domain.Interfaces.Repository;
using Diplom.Infrastructure.Data.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Infrastructure.Data.Repository
{
    class BiographyRepository : BaseRepository<Biography>
    {
        private DatabaseContext context;
        public BiographyRepository(DbContext context) : base(context)
        {
            this.context = context as DatabaseContext;
        }

        public override void Append(Biography item)
        {
            throw new NotImplementedException();
        }

        public override Biography Get(int id)
        {
            return context.Biographies.Include("Author").FirstOrDefault(p => p.Id == id);
        }

        public override IEnumerable<Biography> GetAll(int cId = 0, int aId = 0)
        {
            return context.Biographies.ToList();
        }

        public override void Update(Biography item)
        {
            var biography = Get(item.Id);
            biography.Copy(item);
            context.SaveChanges();
        }
    }
}
