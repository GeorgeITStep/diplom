﻿using Diplom.Domain.Core.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Infrastructure.Data.DTO
{
    public class CommentDto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [MinLength(5)]
        public string Text { get; set; }
        public int BookId { get; set; }
        public int IdentityUserId { get; set; }
    }
}
