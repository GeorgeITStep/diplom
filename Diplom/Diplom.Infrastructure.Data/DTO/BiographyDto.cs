﻿using Diplom.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Infrastructure.Data.DTO
{
    public class BiographyDto
    {
        public int Id { get; set; }
        public DateTime DateOfDeath { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Required]
        public string PlaceOfBirth { get; set; }
        public string Aliases { get; set; }
        [Required]
        [StringLength(25, MinimumLength = 5)]
        public string Language { get; set; }
        [Required]
        public string Occupation { get; set; }
        [Required]
        [MinLength(50)]
        public string Text { get; set; }
        [Required]
        public int AuthorId { get; set; }
    }
}

