﻿using Diplom.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Infrastructure.Data.DTO
{
    public class BookDto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [StringLength(35, MinimumLength = 3)]
        public string Name { get; set; }
        [Required]
        [MinLength(100)]
        public string  Text { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Image { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string PubblishingHouse { get; set; }
        [Required]
        public int Year { get; set; }
        [Required]
        [StringLength(35, MinimumLength = 3)]
        public string Language { get; set; }
        [Required]
        public int Pages { get; set; }
        [Required]
        public int AuthorId { get; set; }
        [Required]
        public int CategoryId { get; set; }
    }
}
