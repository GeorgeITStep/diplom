﻿using Diplom.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Infrastructure.Data.DTO
{
    public class AuthorDto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [StringLength(25, MinimumLength = 4)]
        public string Name { get; set; }
        [Required]
        [StringLength(25, MinimumLength = 4)]
        public string LastName { get; set; }
        [Required]
        public string Img { get; set; }
    }
}
