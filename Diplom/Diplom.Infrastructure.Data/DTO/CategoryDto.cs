﻿using Diplom.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Infrastructure.Data.DTO
{
    public class CategoryDto
    {
        public int Id { get; set; }
        [Required]
        [StringLength(35,MinimumLength =5)]
        public string Name { get; set; }
    }
}
