﻿using Diplom.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Diplom.Domain.Interfaces.Repository
{
    public abstract class BaseRepository<T> : IRepository<T> where T : class
    {
        private DbContext context;


        public BaseRepository(DbContext context)
        {
            this.context = context;
        }
        public void Create(T item)
        {
            context.Entry(item).State = EntityState.Added;
            context.SaveChanges();
        }
        public abstract void Append(T item);
        public void Delete(int id)
        {
            var item = Get(id);
            context.Entry(item).State = EntityState.Deleted;
            context.SaveChanges();
        }

        public IEnumerable<T> Find(Func<T, bool> predicate)
        {
            return context.Set<T>().Where(predicate).ToList();
        }

        public abstract T Get(int id);

        public abstract IEnumerable<T> GetAll(int cId = 0, int aId = 0);
        public abstract void Update(T item);
    }
}
