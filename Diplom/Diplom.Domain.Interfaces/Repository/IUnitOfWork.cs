﻿using Diplom.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Domain.Interfaces.Repository
{
    public interface IUnitOfWork
    {
        BaseRepository<Biography> BiographyRepository { get; }
        BaseRepository<Category> CategoryRepository { get; }
        BaseRepository<Author> AuthorRepository { get; }
        BaseRepository<Book> BookRepository { get; }
        BaseRepository<Comment> CommentRepository { get; }
    }
}
