﻿using AutoMapper;
using Diplom.Domain.Core.Models;
using Diplom.Domain.Interfaces.Repository;
using Diplom.Infrastructure.Data.DTO;
using Diplom.Infrastructure.Data.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Serveces.Interfeces
{
    public class BiographyService
    {
        private IUnitOfWork uow;
        private MapperConfiguration configBiographyToBiographyDto;
        private MapperConfiguration configBiographyDtoToBiography;
        public BiographyService()
        {
            configBiographyToBiographyDto = new MapperConfiguration(cfg => cfg.CreateMap<Biography, BiographyDto>());
            configBiographyDtoToBiography = new MapperConfiguration(cfg => cfg.CreateMap<BiographyDto, Biography>()
            .ForMember("Author", c => c.MapFrom(src => uow.AuthorRepository.Get(src.AuthorId))));
            uow = EFUnitOfWork.Instance;
        }
        public IEnumerable<BiographyDto> GetBiographies()
        {
            var biographies = uow.BiographyRepository.GetAll();
            Mapper mapper = new Mapper(configBiographyToBiographyDto);
            List<BiographyDto> biographiesDto = mapper.Map<List<BiographyDto>>(biographies);

            return biographiesDto;
        }
        public BiographyDto Get(int id)
        {
            var biography = uow.BiographyRepository.Get(id);
            Mapper mapper = new Mapper(configBiographyToBiographyDto);
            BiographyDto biographyDto = mapper.Map<BiographyDto>(biography);
            return biographyDto;
        }
        public BiographyDto GetByAuthorId(int authorId)
        {
            var biography = GetBiographies();
            foreach(var item in biography)
            {
                if(item.AuthorId == authorId)
                {
                    return item;
                }
            }
            return null;
        }
        public void Create(BiographyDto biographyDto)
        {
            Mapper mapper = new Mapper(configBiographyDtoToBiography);
            Biography biography = mapper.Map<Biography>(biographyDto);
            uow.AuthorRepository.Append(biography.Author);
            uow.BiographyRepository.Create(biography);
        }

        public void Delete(int id)
        {
            uow.BiographyRepository.Delete(id);
        }

        public void Update(BiographyDto biographyDto)
        {
            Mapper mapper = new Mapper(configBiographyDtoToBiography);
            Biography biography = mapper.Map<Biography>(biographyDto);
            uow.BiographyRepository.Update(biography);
        }
    }
}
