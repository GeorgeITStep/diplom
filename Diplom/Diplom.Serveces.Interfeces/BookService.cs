﻿using AutoMapper;
using Diplom.Domain.Core.Models;
using Diplom.Domain.Interfaces.Repository;
using Diplom.Infrastructure.Data.DTO;
using Diplom.Infrastructure.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Serveces.Interfeces
{
    public class BookService
    {
        IUnitOfWork uow;
        MapperConfiguration configBookToBookDto;
        MapperConfiguration configBookDtoToBook;
        public BookService()
        {
            configBookToBookDto = new MapperConfiguration(cfg => cfg.CreateMap<Book, BookDto>());

            configBookDtoToBook = new MapperConfiguration(cfg => cfg.CreateMap<BookDto, Book>()
            .ForMember("Author", c => c.MapFrom(src => uow.AuthorRepository.Get(src.AuthorId)))
            .ForMember("Category", c => c.MapFrom(src => uow.CategoryRepository.Get(src.CategoryId))));


            uow = EFUnitOfWork.Instance;
        }
        public IEnumerable<BookDto> GetBooks(int cId = 0, int aId = 0)
        {
            var books = uow.BookRepository.GetAll(cId, aId);
            Mapper mapper = new Mapper(configBookToBookDto);
            List<BookDto> booksDto = mapper.Map<List<BookDto>>(books);

            return booksDto;
        }


        public BookDto Get(int id)
        {
            var book = uow.BookRepository.Get(id);
            Mapper mapper = new Mapper(configBookToBookDto);
            BookDto bookDto = mapper.Map<BookDto>(book);
            return bookDto;
        }

        public void Create(BookDto bookDto)
        {
            Mapper mapper = new Mapper(configBookDtoToBook);
            Book book = mapper.Map<Book>(bookDto);
            uow.BookRepository.Create(book);
        }

        public void Delete(int id)
        {
            uow.BookRepository.Delete(id);
        }

        public void Update(BookDto bookDto)
        {
            Mapper mapper = new Mapper(configBookDtoToBook);
            Book book = mapper.Map<Book>(bookDto);
            uow.BookRepository.Update(book);
        }

        public IEnumerable<BookDto> SelectByName(string name)
        {
            var books = uow.BookRepository.Find(b => b.Name.Contains(name));
            Mapper mapper = new Mapper(configBookToBookDto);
            List<BookDto> booksDto = mapper.Map<List<BookDto>>(books);
            return booksDto;
        }
        public IEnumerable<BookDto> Sort()
        {
            var books = GetBooks().ToList();
            books.Sort((p, q) => (p.Name.CompareTo(q.Name)));
            return books;
        }

    }
}
