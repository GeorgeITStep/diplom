﻿using AutoMapper;
using Diplom.Domain.Core.Models;
using Diplom.Domain.Interfaces.Repository;
using Diplom.Infrastructure.Data.DTO;
using Diplom.Infrastructure.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Serveces.Interfeces
{
    public class AuthorService
    {
        private IUnitOfWork uow;
        private MapperConfiguration configAuthorToAuthorDto;
        private MapperConfiguration configAuthorDtoToAuthor;

        public AuthorService()
        {
            configAuthorToAuthorDto = new MapperConfiguration(cfg => cfg.CreateMap<Author, AuthorDto> ());
            configAuthorDtoToAuthor = new MapperConfiguration(cfg => cfg.CreateMap<AuthorDto, Author> ());

            uow = EFUnitOfWork.Instance;
        }
        public IEnumerable<AuthorDto> GetAuthors()
        {
            var authors = uow.AuthorRepository.GetAll();
            Mapper mapper = new Mapper(configAuthorToAuthorDto);
            List<AuthorDto> authorsDto = mapper.Map<List<AuthorDto>>(authors);

            return authorsDto;
        }

        public AuthorDto Get(int id)
        {
            var author = uow.AuthorRepository.Get(id);
            Mapper mapper = new Mapper(configAuthorToAuthorDto);
            AuthorDto authorDto = mapper.Map<AuthorDto>(author);
            return authorDto;
        }

        public void Create(AuthorDto authorDto)
        {
            Mapper mapper = new Mapper(configAuthorDtoToAuthor);
            Author author = mapper.Map<Author>(authorDto);
            //var idBiography = authorDto.BiographyId;
            //MapperConfiguration config = new MapperConfiguration(cfg => cfg.CreateMap<BiographyDto, Biography>());
            //mapper = new Mapper(config);
            //BiographyService biographyService = new BiographyService();
            //author.Biography = mapper.Map<Biography>(biographyService.Get(idBiography));
            uow.AuthorRepository.Create(author);
        }

        public void Delete(int id)
        {
            uow.AuthorRepository.Delete(id);
        }

        public void Update(AuthorDto authorDto)
        {
            Mapper mapper = new Mapper(configAuthorDtoToAuthor);
            Author author = mapper.Map<Author>(authorDto);
            uow.AuthorRepository.Update(author);
        }
        public IEnumerable<AuthorDto> SelectByLastName(string name)
        {
            var authors = uow.AuthorRepository.Find(a => a.LastName.Contains(name) || a.Name.Contains(name));
            Mapper mapper = new Mapper(configAuthorToAuthorDto);
            List<AuthorDto> authorsDto = mapper.Map<List<AuthorDto>>(authors);
            return authorsDto;
        }
    }
}
