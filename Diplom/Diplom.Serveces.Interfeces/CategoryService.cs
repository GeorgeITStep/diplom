﻿using AutoMapper;
using Diplom.Domain.Core.Models;
using Diplom.Domain.Interfaces.Repository;
using Diplom.Infrastructure.Data.DTO;
using Diplom.Infrastructure.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Serveces.Interfeces
{
    public class CategoryService
    {
        private IUnitOfWork uow;
        MapperConfiguration configCategoryToCategoryDto;
        MapperConfiguration configCategoryDtoToCategory;
        public CategoryService()
        {
            configCategoryToCategoryDto = new MapperConfiguration(cfg => cfg.CreateMap<Category, CategoryDto>());

            configCategoryDtoToCategory = new MapperConfiguration(cfg => cfg.CreateMap<CategoryDto, Category>());

            uow = EFUnitOfWork.Instance;
        }
        public IEnumerable<CategoryDto> GetCategories()
        {
            var categories = uow.CategoryRepository.GetAll();
            Mapper mapper = new Mapper(configCategoryToCategoryDto);
            List<CategoryDto> categoriesDto = mapper.Map<List<CategoryDto>>(categories);
            
            return categoriesDto;
        }
        public CategoryDto Get(int id)
        {
            var category = uow.CategoryRepository.Get(id);
            Mapper mapper = new Mapper(configCategoryToCategoryDto);
            CategoryDto categoryDto = mapper.Map<CategoryDto>(category);
            return categoryDto;
        }

        public void Create(CategoryDto categoryDto)
        {
            Mapper mapper = new Mapper(configCategoryDtoToCategory);
            Category category = mapper.Map<Category>(categoryDto);
            uow.CategoryRepository.Create(category);
        }

        public void Delete(int id)
        {
            uow.CategoryRepository.Delete(id);
        }

        public void Update(CategoryDto categoryDto)
        {
            Mapper mapper = new Mapper(configCategoryDtoToCategory);
            Category category = mapper.Map<Category>(categoryDto);
            uow.CategoryRepository.Update(category);
        }
    }
}
