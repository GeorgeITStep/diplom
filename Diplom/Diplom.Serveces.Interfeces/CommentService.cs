﻿using AutoMapper;
using Diplom.Domain.Core.Models;
using Diplom.Domain.Interfaces.Repository;
using Diplom.Infrastructure.Data.DTO;
using Diplom.Infrastructure.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Serveces.Interfeces
{
    public class CommentService
    {
        private IUnitOfWork uow;
        MapperConfiguration configCommentToCommentDto;
        MapperConfiguration configCommentDtoToComment;
        public CommentService()
        {
            configCommentToCommentDto = new MapperConfiguration(cfg => cfg.CreateMap<Comment, CommentDto>());

            configCommentDtoToComment = new MapperConfiguration(cfg => cfg.CreateMap<CommentDto, Comment>());

            uow = EFUnitOfWork.Instance;
        }
        public IEnumerable<CommentDto> GetComments()
        {
            var comments = uow.CommentRepository.GetAll();
            Mapper mapper = new Mapper(configCommentToCommentDto);
            List<CommentDto> commentsDto = mapper.Map<List<CommentDto>>(comments);

            return commentsDto;
        }
        public CommentDto Get(int id)
        {
            var comment = uow.CommentRepository.Get(id);
            Mapper mapper = new Mapper(configCommentToCommentDto);
            CommentDto commentDto = mapper.Map<CommentDto>(comment);
            return commentDto;
        }

        public void Create(CommentDto commentDto)
        {
            Mapper mapper = new Mapper(configCommentDtoToComment);
            Comment comment = mapper.Map<Comment>(commentDto);
            uow.CommentRepository.Create(comment);
        }

        public void Delete(int id)
        {
            uow.CommentRepository.Delete(id);
        }

        public void Update(CommentDto commentDto)
        {
            Mapper mapper = new Mapper(configCommentDtoToComment);
            Comment comment = mapper.Map<Comment>(commentDto);
            uow.CommentRepository.Create(comment);
        }
    }
}
