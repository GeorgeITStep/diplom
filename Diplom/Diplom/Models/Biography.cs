﻿using Diplom.Infrastructure.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Models.ViewModels
{
    public class Biography
    {
        public int Id { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime DateOfDeath { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Aliases { get; set; }
        public string Language { get; set; }
        public string Occupation { get; set; }
        public string Text { get; set; }
        public Author Author { get; set; }
        public Biography()
        {
        }
        
    }
}
