﻿using Diplom.Infrastructure.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Models.ViewModels
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string PartText { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string PubblishingHouse { get; set; }
        public int Year { get; set; }
        public string Language { get; set; }
        public int Pages { get; set; }
        public Author Author { get; set; }
        public Category Category { get; set; }
        public Book()
        {
        }
        
    }
}
