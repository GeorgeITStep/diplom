﻿using Diplom.Infrastructure.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Models.ViewModels
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Category()
        {
        }
        
    }
}
