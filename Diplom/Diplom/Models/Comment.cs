﻿using Diplom.Infrastructure.Data.DTO;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models.ViewModels
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public Book Book { get; set; }
        public IdentityUser User { get; set; }

        
    }
}