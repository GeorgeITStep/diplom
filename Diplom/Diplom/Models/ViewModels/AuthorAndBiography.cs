﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models.ViewModels
{
    public class AuthorAndBiography
    {
        public Author Author { get; set; }
        public Biography Biography { get; set; }
    }
}