﻿using Diplom.Infrastructure.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models.ViewModels
{
    public class BookAndCategory
    {
        public Book Book = new Book();
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Author> Authors { get; set; }
    }
}