﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models.ViewModels
{
    public class ReadBook
    {
        public string BookName { get; set; }
        public int BookId { get; set; }
        public string Text { get; set; }
        public int Pages { get; set; }
        public ReadBook(string bookName, int pages, int bookId)
        {
            BookName = bookName;
            Pages = pages;
            BookId = bookId;
        }
    }
}