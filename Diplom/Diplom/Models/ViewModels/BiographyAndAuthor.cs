﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models.ViewModels
{
    public class BiographyAndAuthor
    {
        public Biography Biography { get; set; }
        public List<Author> Authors { get; set; }
    }
}