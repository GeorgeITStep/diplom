﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models.ViewModels
{
    public class BooksAndAuthors
    {
        public List<Book> Books { get; set; }
        public List<Author> Authors { get; set; }
    }
}