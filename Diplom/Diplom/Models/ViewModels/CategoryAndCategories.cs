﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models.ViewModels
{
    public class CategoryAndCategories
    {
        public Category Category { get; set; }
        public List<Category> Categories { get; set; }
    }
}