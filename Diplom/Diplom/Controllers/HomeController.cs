﻿using AutoMapper;
using Diplom.Infrastructure.Data.DTO;
using Diplom.Models.ViewModels;
using Diplom.Serveces.Interfeces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Diplom.Controllers
{
    public class HomeController : Controller
    {
        private CategoryService categoryService = new CategoryService();
        private BookService bookService = new BookService();
        public ActionResult Index(int categoryId = 0, int authorId = 0, string name = "", bool sort = false)
        {
            MapperConfiguration config = new MapperConfiguration(cfg => cfg.CreateMap<CategoryDto, Category>());

            Mapper mapper = new Mapper(config);
            var categoriesDto = categoryService.GetCategories();
            var categories = mapper.Map<List<Category>>(categoriesDto);

            List<BookDto> booksDto = new List<BookDto>();
            if (!name.Equals(""))
                booksDto = (List<BookDto>)bookService.SelectByName(name);
            else if(sort)
                booksDto = (List<BookDto>)bookService.Sort();
            else
                booksDto = (List<BookDto>)bookService.GetBooks(categoryId, authorId);
            config = new MapperConfiguration(cfg => cfg.CreateMap<BookDto, Book>());
            mapper = new Mapper(config);
            var books = mapper.Map<List<Book>>(booksDto);
            return View(new BooksAndCategories
            {
                Categories = categories,
                Books = books
            });
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}