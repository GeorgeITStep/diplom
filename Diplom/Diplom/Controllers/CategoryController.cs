﻿using AutoMapper;
using Diplom.Infrastructure.Data.DTO;
using Diplom.Models.ViewModels;
using Diplom.Serveces.Interfeces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Diplom.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        private CategoryService categoryService = new CategoryService();
        private MapperConfiguration config;
        public CategoryController()
        {
            config = new MapperConfiguration(cfg => cfg.CreateMap<CategoryDto, Category>());
        }
        public ActionResult Index()
        {
            Mapper mapper = new Mapper(config);
            var categories = mapper.Map<List<Category>>(categoryService.GetCategories());
            return View(categories);
        }
        public ActionResult GetCategory(int id)
        {
            Mapper mapper = new Mapper(config);
            var category = mapper.Map<Category>(categoryService.Get(id));
            return View(category);
        }
    }
}