﻿using AutoMapper;
using Diplom.Infrastructure.Data.DTO;
using Diplom.Models.ViewModels;
using Diplom.Serveces.Interfeces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Diplom.Controllers
{
    public class BookController : Controller
    {
        // GET: Book
        private BookService bookService = new BookService();
        AuthorService authorService = new AuthorService();
        MapperConfiguration config;
        MapperConfiguration configAuthor;

        public BookController()
        {
            configAuthor = new MapperConfiguration(cfg => cfg.CreateMap<AuthorDto, Author>());
            Mapper mapperAuthor = new Mapper(configAuthor);
            config = new MapperConfiguration(cfg => cfg.CreateMap<BookDto, Book>()
            .ForMember("Author", c => c.MapFrom(src => mapperAuthor.Map<Author>(authorService.Get(src.AuthorId)))));

        }
        public ActionResult Index(int categoryId = 0, int authorId = 0)
        {
            var b = bookService.GetBooks(categoryId, authorId);
            Mapper mapper = new Mapper(config);
            var books = mapper.Map<List<Book>>(b);
            return View(books);
        }
        public ActionResult Book(int id)
        {
            Mapper mapper = new Mapper(config);
            var book = mapper.Map<Book>(bookService.Get(id));
            return View(book);
        }
        public ActionResult Read(int? id, int p = 1)
        {
            p--;
            if (id is null)
                return RedirectToAction("../");
            Mapper mapper = new Mapper(config);
            var book = mapper.Map<Book>(bookService.Get((int)id));
            var readBook = new ReadBook(book.Name, (book.Text.Length / 7000) + 1, book.Id);
            int step = 7000;
            if (book.Text.Length < p * step + step)
                step = book.Text.Length - p * step;
            readBook.Text = book.Text.Substring(p * 7000, step);
            int idx = step - 1;
            while (readBook.Text[idx-1].ToString() != " ")
            {
                idx--;
                readBook.Text = readBook.Text.Substring(0, idx);
            }
            if (p != 0)
            {
                idx = 0;
                string s = "";
                while (book.Text[p * 7000 - (idx + 1)].ToString() != " ")
                {
                    idx++;
                    s = book.Text[p * 7000 - idx].ToString();
                    readBook.Text = readBook.Text.Insert(0, s);
                }
            }
            return View(readBook);
        }
    }
}