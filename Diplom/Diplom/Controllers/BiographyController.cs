﻿using AutoMapper;
using Diplom.Infrastructure.Data.DTO;
using Diplom.Models.ViewModels;
using Diplom.Serveces.Interfeces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Diplom.Controllers
{
    public class BiographyController : Controller
    {
        // GET: Biography
        private BiographyService biographyService = new BiographyService();
        AuthorService authorService = new AuthorService();
        MapperConfiguration configBiography;
        MapperConfiguration configAuthor;
        public BiographyController()
        {
            configAuthor = new MapperConfiguration(cfg => cfg.CreateMap<AuthorDto, Author>());
            Mapper mapperAuthor = new Mapper(configAuthor);
            configBiography = new MapperConfiguration(cfg => cfg.CreateMap<BiographyDto, Biography>()
            .ForMember("Author", c => c.MapFrom(src => mapperAuthor.Map<Author>(authorService.Get(src.AuthorId)))));

        }
        public ActionResult Index()
        {
            Mapper mapper = new Mapper(configBiography);
            var biographies = mapper.Map<List<Biography>>(biographyService.GetBiographies());
            return View(biographies);
        }
        public ActionResult Biography(int? authorId, int id = 0)
        {
            BiographyDto biographyDto = new BiographyDto();
            if (id != 0)
                biographyDto = biographyService.Get(id);
            else
                biographyDto = biographyService.GetByAuthorId((int)authorId);
            if (biographyDto is null)
                return RedirectToAction("../Home/Index");
            Mapper mapper = new Mapper(configBiography);
            var biography = mapper.Map<Biography>(biographyDto);

            return View(biography);
        }
    }
}