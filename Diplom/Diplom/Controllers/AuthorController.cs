﻿using AutoMapper;
using Diplom.Infrastructure.Data.DTO;
using Diplom.Models.ViewModels;
using Diplom.Serveces.Interfeces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Diplom.Controllers
{
    public class AuthorController : Controller
    {
        // GET: Author
        private AuthorService authorService = new AuthorService();
        private MapperConfiguration config;
        public AuthorController()
        {
            config = new MapperConfiguration(cfg => cfg.CreateMap<AuthorDto, Author>());
        }
        public ActionResult Index(string lastName = "")
        {
            Mapper mapper = new Mapper(config);
            List<AuthorDto> authorsDto = new List<AuthorDto>();
            if (!lastName.Equals(""))
                authorsDto = (List<AuthorDto>)authorService.SelectByLastName(lastName);
            else
                authorsDto = (List<AuthorDto>)authorService.GetAuthors();
            var authors = mapper.Map<List<Author>>(authorsDto);
            return View(authors);
        }
        public ActionResult Author(int id)
        {
            Mapper mapper = new Mapper(config);
            var author = mapper.Map<Author>(authorService.Get(id));
            return View(author);
        }
    }
}