﻿using AutoMapper;
using Diplom.Infrastructure.Data.DTO;
using Diplom.Models.ViewModels;
using Diplom.Serveces.Interfeces;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Diplom.Controllers
{
    [Authorize(Roles = Diplom.Models.RoleConfig.ADMIN)]
    public class AdminController : Controller
    {
        BookService bookService = new BookService();
        AuthorService authorService = new AuthorService();
        CategoryService categoryService = new CategoryService();
        BiographyService biographyService = new BiographyService();

        MapperConfiguration configAuthorDtoToAuthor;
        MapperConfiguration configCategoryDtoToCategory;
        MapperConfiguration configBookDtoToBook;
        MapperConfiguration configBiographyDtoToBiography;
        public AdminController()
        {
            configAuthorDtoToAuthor = new MapperConfiguration(cfg => cfg.CreateMap<AuthorDto, Author>());
            configCategoryDtoToCategory = new MapperConfiguration(cfg => cfg.CreateMap<CategoryDto, Category>());
            configBookDtoToBook = new MapperConfiguration(cfg => cfg.CreateMap<BookDto, Book>());
            configBiographyDtoToBiography = new MapperConfiguration(cfg => cfg.CreateMap<BiographyDto, Biography>());

        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateBook(Book book)
        {
            Mapper mapper = new Mapper(configAuthorDtoToAuthor);
            var authors = mapper.Map<List<Author>>(authorService.GetAuthors());
            mapper = new Mapper(configCategoryDtoToCategory);
            var categories = mapper.Map<List<Category>>(categoryService.GetCategories());
            if (book is null)
                book = new Book();

            return View(new BookAndCategory
            {
                Book = book,
                Categories = categories,
                Authors = authors
            });
        }
        [HttpPost]
        public ActionResult CreateBook(BookDto book, string path)
        {
            if (!ModelState.IsValid && path == "" && Path.GetExtension(path) != ".txt")
            {
                if (book.Description.Length > 400)
                {
                    book.Description = "";
                }
                return RedirectToAction("CreateBook", book);
            }
            using (StreamReader sr = new StreamReader(path, Encoding.Default))
            {
                book.Text = sr.ReadToEnd();
            }
            bookService.Create(book);
            return RedirectToAction("../Home");
        }
        public ActionResult EditBook(int? id)
        {
            if (id is null)
                return RedirectToAction("../");
            Mapper mapper = new Mapper(configBookDtoToBook);
            var book = mapper.Map<Book>(bookService.Get((int)id));
            return View(book);
        }
        [HttpPost]
        public ActionResult EditBook(BookDto book)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("EditBook", new { id = book.Id });
            bookService.Update(book);
            return RedirectToAction("../Home");
        }
        public ActionResult DeleteBook(int? id)
        {
            if (id is null)
                return RedirectToAction("../");
            bookService.Delete((int)id);
            return RedirectToAction("../Book/index");
        }


        public ActionResult CreateCategory(Category category)
        {
            Mapper mapper = new Mapper(configCategoryDtoToCategory);
            var categories = mapper.Map<List<Category>>(categoryService.GetCategories());
            if (category is null)
                category = new Category();
            return View(new CategoryAndCategories
            {
                Category = category,
                Categories = categories
            });
        }
        [HttpPost]
        public ActionResult CreateCategory(CategoryDto category)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("CreateCategory", category);
            categoryService.Create(category);
            return RedirectToAction("../Category/index");
        }
        public ActionResult EditCategory(int? id)
        {
            if (id is null)
                return RedirectToAction("../");
            Mapper mapper = new Mapper(configCategoryDtoToCategory);
            var category = mapper.Map<Category>(categoryService.Get((int)id));
            return View(category);
        }
        [HttpPost]
        public ActionResult EditCategory(CategoryDto category)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("EditCategory", new { id = category.Id });
            categoryService.Update(category);
            return RedirectToAction("../Category/index");
        }
        public ActionResult DeleteCategory(int? id)
        {
            if (id is null)
                return RedirectToAction("../");
            categoryService.Delete((int)id);
            return RedirectToAction("../Category/index");
        }

        public ActionResult CreateAuthor(Author author)
        {
            if (author is null)
                return View(new Author());

            return View(author);
        }
        [HttpPost]
        public ActionResult CreateAuthor(AuthorDto author)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("CreateAuthor", author);
            authorService.Create(author);
            return RedirectToAction("../Author/index");
        }
        public ActionResult EditAuthor(int? id)
        {
            if (id is null)
                return RedirectToAction("../");
            Mapper mapper = new Mapper(configAuthorDtoToAuthor);
            var author = mapper.Map<Author>(authorService.Get((int)id));
            return View(author);
        }
        [HttpPost]
        public ActionResult EditAuthor(AuthorDto author)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("EditAuthor", new { id = author.Id });
            authorService.Update(author);
            return RedirectToAction("../Author/index");
        }
        public ActionResult DeleteAuthor(int? id)
        {
            if (id is null)
                return RedirectToAction("../");
            authorService.Delete((int)id);
            return RedirectToAction("../Author/index");
        }

        public ActionResult CreateBiography(Biography biography, int? authorId)
        {
            Mapper mapper = new Mapper(configAuthorDtoToAuthor);
            var authors = mapper.Map<List<Author>>(authorService.GetAuthors());
            if (authorId != null)
                biography.Author = new Author{ Id = (int)authorId};

            return View(new BiographyAndAuthor
            {
                Biography = biography,
                Authors = authors
            });
        }
        [HttpPost]
        public ActionResult CreateBiography(BiographyDto biography)
        {
            
            if (!ModelState.IsValid)
                return RedirectToAction("CreateBiography", biography);
            if (biography.DateOfDeath.Ticks == 0)
            {
                biography.DateOfDeath = new DateTime(9999, 12, 31);
            }
            biographyService.Create(biography);
            return RedirectToAction("../Home");
        }
        public ActionResult EditBiography(int? id)
        {
            if (id is null)
                return RedirectToAction("../");
            Mapper mapper = new Mapper(configBiographyDtoToBiography);
            var biography = mapper.Map<Biography>(biographyService.Get((int)id));
            if(biography.DateOfDeath.Year == 9999)
            {
                biography.DateOfDeath = new DateTime(0001, 01, 01);
            }
            return View(biography);
        }
        [HttpPost]
        public ActionResult EditBiography(BiographyDto biography)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("EditBiography", new { id = biography.Id });
            if (biography.DateOfDeath.Year == 1)
            {
                biography.DateOfDeath = new DateTime(9999, 12, 31);
            }
            biographyService.Update(biography);
            return RedirectToAction("Biography", "Biography", new { id = biography.Id });
        }
        public ActionResult DeleteBiography(int? id)
        {
            if (id is null)
                return RedirectToAction("../");
            biographyService.Delete((int)id);
            return RedirectToAction("../Home");
        }

    }
}